function changePlan() {
    let togglePlan = document.getElementById("change-plan")
    let annualPricing = document.getElementsByClassName("annually-pricing")
    let monthlyPricing = document.getElementsByClassName("monthly-pricing")

    for (let index = 0; index < annualPricing.length; index++) {
        if (togglePlan.checked == true) {
            annualPricing[index].style.display = "none"
            monthlyPricing[index].style.display = "flex"
        } else if (togglePlan.checked == false) {
            annualPricing[index].style.display = "flex"
            monthlyPricing[index].style.display = "none"
        }
    }
}
